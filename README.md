Joint project with Sam Chow (Bristol).

Estimate the number of Fourier coefficients necessary to distinguish two
modular eigenforms mod a prime ideal.
